import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmplacementTarifComponent } from './emplacement-tarif.component';

describe('EmplacementTarifComponent', () => {
  let component: EmplacementTarifComponent;
  let fixture: ComponentFixture<EmplacementTarifComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmplacementTarifComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmplacementTarifComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
