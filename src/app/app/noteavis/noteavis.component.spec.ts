import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NoteavisComponent } from './noteavis.component';

describe('NoteavisComponent', () => {
  let component: NoteavisComponent;
  let fixture: ComponentFixture<NoteavisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NoteavisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NoteavisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
