import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PromotionRemisesComponent } from './promotion-remises.component';

describe('PromotionRemisesComponent', () => {
  let component: PromotionRemisesComponent;
  let fixture: ComponentFixture<PromotionRemisesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromotionRemisesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PromotionRemisesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
