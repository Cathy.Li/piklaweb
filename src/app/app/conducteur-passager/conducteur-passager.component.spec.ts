import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConducteurPassagerComponent } from './conducteur-passager.component';

describe('ConducteurPassagerComponent', () => {
  let component: ConducteurPassagerComponent;
  let fixture: ComponentFixture<ConducteurPassagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConducteurPassagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConducteurPassagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
