import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './app/login/login.component';
import { EmplacementTarifComponent } from './app/emplacement-tarif/emplacement-tarif.component';
import { ConducteurPassagerComponent } from './app/conducteur-passager/conducteur-passager.component';
import { ReservationComponent } from './app/reservation/reservation.component';
import { VehiculesComponent } from './app/vehicules/vehicules.component';
import { CommandesComponent } from './app/commandes/commandes.component';
import { NoteavisComponent } from './app/noteavis/noteavis.component';
import { NotificationComponent } from './app/notification/notification.component';
import { FaqComponent } from './app/faq/faq.component';
import { PromotionRemisesComponent } from './app/promotion-remises/promotion-remises.component';
import { AssistanceComponent } from './app/assistance/assistance.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    EmplacementTarifComponent,
    ConducteurPassagerComponent,
    ReservationComponent,
    VehiculesComponent,
    CommandesComponent,
    NoteavisComponent,
    NotificationComponent,
    FaqComponent,
    PromotionRemisesComponent,
    AssistanceComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
