import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './app/login/login.component';
import { EmplacementTarifComponent } from './app/emplacement-tarif/emplacement-tarif.component';
import { ConducteurPassagerComponent } from './app/conducteur-passager/conducteur-passager.component';
import { ReservationComponent } from './app/reservation/reservation.component';
import { VehiculesComponent } from './app/vehicules/vehicules.component';
import { CommandesComponent } from './app/commandes/commandes.component';
import { NoteavisComponent } from './app/noteavis/noteavis.component';
import { NotificationComponent } from './app/notification/notification.component';
import { FaqComponent } from './app/faq/faq.component';
import { PromotionRemisesComponent } from './app/promotion-remises/promotion-remises.component';
import { AssistanceComponent } from './app/assistance/assistance.component';

const routes: Routes = [
  {path:"login", component:LoginComponent}, 

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
